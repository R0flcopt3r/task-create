# task-create

Creates a C++ folder structure with a name format for "Task [nr] prosject name". Written in bash.

It creates the folders src, bin, assets, headers and LaTeX. It also creates a file named plan.md, where you
can write a plan for how the program should be written. It also creates a makefile for easy compiling,
note it doesn't have a header folder entry.

It also creates a LaTeX folder and a script 'cpp-latex.sh' that adds the cpp file to a LaTeX
document and compiles it as pdf. 



## Usage

Place it into the folder where you wish to create your task folders.

Run the script. It creates a new task folder where the number is the highest number of the folders
already created. If no folders are created it will create a folder with the number 1.

You will also be asked to enter a program name. This is optional.

To manually set a number use the -n switch, followed by a number.

`./newtask.sh -n 4`	will create the folder 'Task 4'.  
`./newtask.sh`		will create a folder based on the folders already existing.

### Example folder stucture:
After newtask.sh and cpp-latex.sh has ran.

    Task 3, example
    ├── assets/
    ├── bin/
    │   └── main
    ├── cpp-latex.sh
    ├── example.pdf
    ├── headers/
    ├── LaTeX/
    │   ├── example.aux
    │   ├── example.fdb_latexmk
    │   ├── example.fls
    │   ├── example.log
    │   ├── example.pdf
    │   └── example.tex
    ├── src
    │   ├── example.cpp
    │   └── makefile
    └── Plan.md
