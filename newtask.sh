#!/bin/bash

# Program to initialize a new c++ week assignment project folder
# programs are named in the form of "task number, program name"


task="Task" 

						    # Find existing folders and adds 
						    #	their number into an array:
ar=($(ls | grep "$task" | awk -F ' ' '{print $2}' | awk -F ',' '{print $1}'))

autoName(){					    # Automatic name. Sets the name 
    max=0					    #	based on the highest number 
    for v in ${ar[@]}; do			    #	in the array ar.
	if (( $v > $max )); then max=$v; fi;
    done
    num=$(($max+1))
    folderName="$task $num$progName"		    # sets the root folder name.
}

    mkFolder(){					    # Creates folder and subfolders.
    mkdir -p "$folderName"/src "$folderName"/bin "$folderName"/headers "$folderName"/assets 
    mkdir -p "$folderName"/LaTeX
    echo "$folderName is created"
}

						    # Create the makefile, note 
makeFile(){					    #	the exclusion of a headers folder:
    cat << EOS > "$folderName"/src/makefile
bDIR=../bin
sDIR=./
CC=g++
LFAGS=

main: \$(sDIR)/*.cpp
	\$(CC) -o \$(bDIR)/main \$(sDIR)/*.cpp \$(LFAGS)
EOS
echo "makefile is created"

}
						    # makes a markdown file where 
plan(){						    #	you can write out a plan for your program. 
cat << EOS > "$folderName"/Plan.md
# $folderName

This is a plan for how the program should be made.

EOS
echo "plan.md is created"

}

mkMain(){					    # Creates the main .cpp file.
    cat << EOS > "$folderName/src/$name.cpp"
#include <iostream>
using namespace std;


int main(){

}
EOS
echo "$name.cpp is created"
}

manName(){					    # Manual folder numbering
    folderName="$task $OPTARG$progName"
    if [ -e "./$folderName" ]; then
	echo "$folderName"
	echo "Folder exist"
	exit
    else
	echo "$folderName"
    fi
}
						    # Create cpp-latex script. Using EOS as
mkLatex(){					    #	End of script, because nesting.
    cat << EOS > "$folderName/cpp-latex.sh"
#!/bin/bash

# This program adds a C++ file to a LaTeX template
# document, compiles it and copies it out.

progName=\$(ls ./src | grep .cpp | awk -F '.' '{print \$1}')      # Finds the cpp file name.
								  # Creates a LaTeX document
                                                                  #   from a template:
mkLatex(){
    cat << EOF > ./LaTeX/"\$progName".tex
\documentclass{article}
\usepackage[utf8]{inputenc}
\usepackage{listings}
\usepackage{color}
\usepackage[margin=0.5in]{geometry}

\definecolor{dkgreen}{rgb}{0,0.6,0}
\definecolor{gray}{rgb}{0.5,0.5,0.5}
\definecolor{mauve}{rgb}{0.58,0,0.82}

\lstset{frame=tb,
    language=C++,
    aboveskip=3mm,
    belowskip=3mm,
    showstringspaces=false,
    columns=flexible,
    basicstyle={\\small\\ttfamily},
    numbers=none,
    numberstyle=\\tiny\\color{gray},
    keywordstyle=\\color{blue},
    commentstyle=\\color{dkgreen},
    stringstyle=\\color{mauve},
    breaklines=true,
    breakatwhitespace=true,
    tabsize=3
    extendedchars=true,
    literate={å}{{\\aa}}1 {æ}{{\\ae}}1 {ø}{{\\o}}1,
}
\\begin{document}

\\begin{lstlisting}
EOF
    cat ./src/"\$progName".cpp >> ./LaTeX/"\$progName".tex        # Appends the C++ file to doc.

    cat << EOF >> ./LaTeX/"\$progName".tex			  # Ends the LaTeX document.
\end{lstlisting}
\end{document}

EOF
}

mkLatex
cd ./LaTeX                                                      # Changes working directory to
latexmk -pdf                                                    #   LaTeX document and compiles
                                                                
cp "\$progName".pdf ../.					# Copies finished pdf back to root

EOS

chmod +x "./$folderName/cpp-latex.sh"		    # Gives the script execution rights
echo "cpp-latex.sh is created and has execute right"
}
						    # This function creates a script to insert

secFunction(){					    # Includes all the various functions.
    mkFolder					    # Creates folders and subfolders.
    makeFile		    			    # Creates makefile.
    plan					    # Creates the plan document.
    mkMain					    # Creates the main cpp file
    mkLatex
}

echo "Write the name of the program, keep blank if none:" 
read name
						    # Checks the name entered
if [ -z $name ]; then				    #	if empty, set name to main
    progName=""					    #	if not, set the program name
    name="main"					    #	to name
else
    progName=", $name"
fi
echo ""

while getopts "n:" opt; do			    # Option to set your own number 
    case $opt in				    #	in case you skip an assignment.
	n)
	    manName				    # Set the number manually to getopts argument
	    secFunction				    # Runs secondary functions
	    exit
	    ;;
    esac
done

autoName					    # Runs the autoname function
secFunction					    # Runs secondary functions
